var extractContent = function (s) {
  var span = document.createElement('span');
  span.innerHTML = s;
  return span.textContent || span.innerText;
};
var updateBG = function(){
  $('[data-background]').each(function(){
    var target =$(this).find('img').first();
    var bgURL= 'url('+target.attr('src')+')';
    $(this).css('background-image',bgURL);
    target.hide();
  });
}
window.done[0]=true;
window.done[1]=true;
window.done[2]=true;
window.done[3]=true;
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
$(function(){
  
  var settingsA = {
  "async": true,
  "crossDomain": true,
  "url": "/wordpress/index.php/wp-json/wp/v2/posts?_embed&categories=3&order=desc",
  "method": "GET",
  "headers": {}
}
 var MONTH = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
$.ajax(settingsA).done(function (response) {
  var articlesJSON = response;
  var articleContainer = $('[data-id="articles"]');
  var arts = '';
  articlesJSON.forEach(function(it,ind){
    let img = "/wordpress/wp-content/uploads/"+it._embedded['wp:featuredmedia']['0'].media_details.file;
    let img_alt=it._embedded['wp:featuredmedia']['0'].title;
    let title= it.title.rendered;
    let id = it.id;
    let sd= it.excerpt.rendered.substring(0,150);
    let dt = new Date(it.date).getDate()+" "+MONTH[new Date(it.date).getUTCMonth()-1]+' '+new Date(it.date).getFullYear();
    if(it.excerpt.rendered.length > sd.length)
      sd+='...';
    var template= '<div class="row border-bottom pt-3 pb-3"> <div class="col-sm-3"> <img class="img-fluid mb-lg-0 mb-3" src="'+img+'" alt="'+img_alt+'"> </div><div class="col-sm-9 align-self-sm-center"> <h3>'+title+'</h3><small><b>Posted on '+dt+'</b></small><p class="lead mb-1">'+sd+'</p><a href="article.html?id='+id+'" class="link">READ MORE</a></div></div>';
    
    articleContainer.append(template);
  });
  
  
  window.done[4]=true;
  updateBG();
});

});