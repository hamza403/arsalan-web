var extractContent = function (s) {
  var span = document.createElement('span');
  span.innerHTML = s;
  return span.textContent || span.innerText;
};
var updateBG = function(){
  $('[data-background]').each(function(){
    var target =$(this).find('img').first();
    var bgURL= 'url('+target.attr('src')+')';
    $(this).css('background-image',bgURL);
    target.hide();
  });
}
window.done[0]=true;
window.done[1]=true;
window.done[2]=true;
window.done[3]=true;
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
$(function(){
  
  var settingsA = {
  "async": true,
  "crossDomain": true,
  "url": "/wordpress/index.php/wp-json/wp/v2/posts?_embed&categories=2&order=desc",
  "method": "GET",
  "headers": {}
}
var htmlDecode = function (input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}
$.ajax(settingsA).done(function (response) {
  var articlesJSON = response;
  var articleContainer = $('[data-id="certificates"]');
  var arts = '';
  console.log(response);
  articlesJSON.forEach(function(it,ind){
    let img = "/wordpress/wp-content/uploads/"+it._embedded['wp:featuredmedia']['0'].media_details.file;
    let img_alt=it._embedded['wp:featuredmedia']['0'].title;
    let title= it.title.rendered;
    let id = it.id;
    let sd= it.content.rendered;
    if(sd==null || sd == 'undefined' || sd=="")
      sd='';
//    
    var MONTH = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    let date= new Date(it.date);
    let fuld = date.getDate() + ' ' + MONTH[date.getMonth()]+ ' ' + date.getFullYear();
    let fct = htmlDecode(it.metadata.facts[0]).split(',');
    
//    
//    
    let st = '';
    fct.map((f)=>{
      let v = f.split(':')[0];
      let k = f.split(':')[1];
      let p = (v=='') ? 'pl-0' : '';
      
      if(k==null || k == 'undefined' || k=="" || k==undefined)
      k='';  
      st+='<div class="stat"> <span class="fact" data-id="hb-fact-1">'+v+'</span> <span class="text text-capitalize '+p+'" data-id="hb-text-1">'+k+'</span></div>';
    });
    
    var template= '<div class="row pt-2 pb-2 border-bottom"> <div class="col-sm-3 mb-sm-0 mb-2"> <img src="'+img+'" alt="'+img_alt+'" class="img-fluid" style="max-height:150px; display:block; margin:0px auto;"> </div><div class="col-sm-9 align-self-center"> <h3>'+title+'</h3><small><b>Achieved on '+fuld+'</b></small> <p class="lead">'+sd+'</p><div class="stats">'+st+'</div></div></div>';
    
    articleContainer.append(template);
  });
  
  
  window.done[4]=true;
  updateBG();
});

});