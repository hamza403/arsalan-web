var extractContent = function (s) {
  var span = document.createElement('span');
  span.innerHTML = s;
  return span.textContent || span.innerText;
};
var updateBG = function(){
  $('[data-background]').each(function(){
    var target =$(this).find('img').first();
    var bgURL= 'url('+target.attr('src')+')';
    $(this).css('background-image',bgURL);
    target.hide();
  });
}
window.done[0]=true;
window.done[1]=true;
window.done[2]=true;
window.done[4]=true;
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
$(function(){
  var projId = getUrlParameter('id');
  if(projId===undefined){
    window.location = "./projects.html";
  }
  console.log(projId);
  var settings = {
  "async": true,
  "crossDomain": true,
  "url":"/wordpress/index.php/wp-json/wp/v2/posts/"+projId+"?_embed",
  "method": "GET",
  "headers": {}
}

$.ajax(settings).done(function (response) {
  var proj = response;
 
  let img, img_alt;
  if (proj._embedded['wp:featuredmedia']['0'] != null && proj._embedded['wp:featuredmedia']['0'] != 'undefined') {
    img = "/wordpress/wp-content/uploads/"+proj._embedded['wp:featuredmedia']['0'].media_details.file;
    img_alt = proj._embedded['wp:featuredmedia']['0'].title;
  }
  else{
    img = 'img/wip.jpg';
    img_alt = 'No Image';
  }
  let txt = proj.title.rendered;
  let skil = proj._embedded['wp:term']['1'];
  let id = proj.id;
  let skilz='';
  skil.map((sk)=>{skilz+='<span class="skill">'+sk.name+'</span>'});
  $("[data-id='project-name']").text(txt);
  $("[data-id='skills']").html(skilz);
  $("[data-id='description']").html(proj.content.rendered);
//  $('.header-bg').css('background-image','url('+img+')');
   $("[data-id='featured-img']").attr('src',img);
  $("[data-id='featured-img']").attr('alt',img_alt);
  
  window.done[3]=true;
});

});