var extractContent = function (s) {
  var span = document.createElement('span');
  span.innerHTML = s;
  return span.textContent || span.innerText;
};
var htmlDecode= function(input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}
var updateBG = function(){
  $('[data-background]').each(function(){
    var target =$(this).find('img').first();
    var bgURL= 'url('+target.attr('src')+')';
    $(this).css('background-image',bgURL);
    target.hide();
  });
}
$(function(){
  
var balanceHeight = function(parent,container=null,offset=0){
  var maxHeight = 0;
  if(container==null)
  var item = parent.children();
  else
    var item = container.children();
  item.each(function (i, el) {
    if ($(this).height() > maxHeight)
      maxHeight = $(this).height();
  });
  var maxItemHeight = maxHeight + offset + 'px';
  item.css('height', maxItemHeight);
  if(container)
    container.css('height', maxItemHeight);
}  
  
var exp_callback=function(data){
  var exps = '';
  
  var MONTH = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
  data.forEach(function(exp,ind){
    var start_date_yr = exp.metadata.start_date.toString().substring(0,4);
    var end_date_yr = exp.metadata.end_date.toString().substring(0,4);
    var start_date_month = MONTH[parseInt(exp.metadata.start_date.toString().substring(4,6))-1];
    var end_date_month = exp.metadata.end_date.toString().substring(4,6);
    
    if(end_date_month=='')
      end_date_month='Present';
    else
      end_date_month=MONTH[parseInt(end_date_month)-1];
    var ofp= start_date_month+" "+start_date_yr+" - "+end_date_month+" "+end_date_yr;
    let cn = htmlDecode(exp.title.rendered).split(' | ')[0];
    let ds = htmlDecode(exp.title.rendered).split(' | ')[1];
    let disc= exp.metadata.short_descr.toString().slice(0,100);
    if(exp.metadata.short_descr.toString().length > disc.length)
    disc+='...';
    let template='<div class="experience"> <div class="col-lg-6 offset-lg-3"> <div class="row"> <div class="row no-gutters w-100"> <div class="year col-lg-1 d-none"> <span>'+start_date_yr+'</span> </div><div class="content col-lg-12 text-center"> <h3 class="company-name">'+cn+'</h3> <a href="./experience.html?id='+exp.id+'" class="link">See Details</a> <h4 class="designation mt-2">'+ds+'</h4><span><b>'+ofp+'</b></span> <p class="d-none">'+disc+'</p></div></div></div></div></div>';
    exps+=template;
  });
    //START EXPERIENCE SLIDER//
    
  var expSlider = $('#experience-slider');
  var expSliderItems = expSlider.find('.items');
  expSliderItems.append(exps);
  var expSliderItem = expSliderItems.find('.experience');
  $(expSliderItem[expSliderItem.length-1]).addClass('active');
  var expSliderControl = expSlider.find('.controls');

  var updateControls = function(){
    var expSliderItemActive = expSliderItems.find('.experience.active');
    var expSliderControl = expSlider.find('.controls');
    var prevYear=expSliderItemActive.prev().find('.year span').text();
    
    var nextYear=expSliderItemActive.next().find('.year span').text();
    console.log(prevYear);
    console.log(nextYear);
    
    expSliderControl.find('.previous .year').text(prevYear);
    if(prevYear==''){
      expSliderControl.find('.previous').addClass('disable');
    }
    else{
      expSliderControl.find('.previous').removeClass('disable');
    }
    expSliderControl.find('.next .year').text(nextYear);
    if(nextYear==''){
      expSliderControl.find('.next').addClass('disable');
    }
    else{
      expSliderControl.find('.next').removeClass('disable');
    }
  }
  
  balanceHeight(expSlider,expSliderItems,58);
  var expSliderItemActive = expSliderItems.find('.experience.active');
  updateControls();
  
  expSliderControl.find('.next').on('click', function () {
    expSliderItemActive = expSliderItems.find('.experience.active');
    if (expSliderItemActive.index() < expSliderItems.children().length - 1){
      expSliderItemActive.removeClass('active').addClass('prev').next().addClass('active');
      
      updateControls();
    }
  });
  expSliderControl.find('.previous').on('click', function () {
    expSliderItemActive = expSliderItems.find('.experience.active');
    if (expSliderItemActive.index() > 0){
      expSliderItemActive.removeClass('active').prev().removeClass('prev').addClass('active');
      updateControls();
    }
  });  
  
  //END EXPERIENCE SLIDER//
  window.done[2]=true;
  };
  var settings = {
  "async": true,
  "crossDomain": true,
  "url": "/wordpress/index.php/wp-json/wp/v2/posts?categories=4&order=asc",
  "method": "GET",
  "headers": {}
}
$.ajax(settings).done(function (response) {
  let data = response;
  if(data != null)
  exp_callback(data);
  
});
  var Plimit=$("[data-id='projects']").attr('data-limit');
//  var settingsP = {
//  "async": true,
//  "crossDomain": true,
//  "url": "/wordpress/index.php/wp-json/wp/v2/posts?_embed&categories=5&per_page="+Plimit,
//  "method": "GET",
//  "headers": {}
//}
  window.done[3]=true;
//$.ajax(settingsP).done(function (response) {
//  console.log(response);
//  var proj = response;
//  var projs = '';
//  proj.forEach(function(it,ind){
//    let img, img_alt;
//  if (it._embedded['wp:featuredmedia']['0'] != null && it._embedded['wp:featuredmedia']['0'] != 'undefined') {
//    img = "/wordpress/wp-content/uploads/"+it._embedded['wp:featuredmedia']['0'].media_details.file;
//    img_alt = it._embedded['wp:featuredmedia']['0'].title;
//  }
//  else{
//    img = 'img/wip.jpg';
//    img_alt = 'No Image';
//  }
//  let txt = it.title.rendered;
//  let skil = it._embedded['wp:term']['1'];
//  let id = it.id;
//  let skilz='';
//  skil.map((sk)=>{skilz+='<span class="skill">'+sk.name+'</span>'});
//  let template='<div class="project"> <div class="project-image" data-background> <img src="'+img+'" data-background alt="'+img_alt+'"> </div><div class="project-text"> <div><h4>'+txt+'</h4> <div class="skills">'+skilz+'</div></div><div><a href="project.html?id='+id+'" class="link">SEE DETAILS</a></div></div></div>';
//  projs+=template;
//  });
//  $('[data-id="projects"]').html(projs);
////  
//  window.done[3]=true;
//  updateBG();
//  balanceHeight($('[data-id="projects"]'),null,60);
////  
//});
  
  var settingsA = {
  "async": true,
  "crossDomain": true,
  "url": "/wordpress/index.php/wp-json/wp/v2/posts?_embed&categories=3&order=desc&per_page=4",
  "method": "GET",
  "headers": {}
}

$.ajax(settingsA).done(function (response) {
  
  let articlesJSON = response;
  let articles='';
  let articleContainer= $('[data-id="articles"]');
  articleContainer.html('');
  let classes=['main','first','second','third'];
  classes.forEach(function(it,ind){
    let ca = articlesJSON[ind];
    if(ca != null){let cls= (ind==0) ? '' : 'secondary';
    let img = "/wordpress/wp-content/uploads/"+ca._embedded['wp:featuredmedia']['0'].media_details.file;
    let img_alt=ca._embedded['wp:featuredmedia']['0'].title;
    let title= ca.title.rendered;
    let id = ca.id;
    let sd= ca.excerpt.rendered.substring(0,80);
    if(ca.excerpt.rendered.length > sd.length)
      sd+='...';
    var template= '<div class="'+it+'"><a href="article.html?id='+id+'" class="dull-link"> <article class="'+cls+'"> <div class="article-img" data-background> <img src="'+img+'" alt="'+img_alt+'"> </div><div class="caption"> <h4>'+title+'</h4> <p>'+sd+'</p></div></article> </a></div>';
//    
    articleContainer.append(template);}
  });
  updateBG();
    window.done[4]=true;
});

});
