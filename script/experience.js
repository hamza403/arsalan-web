var extractContent = function (s) {
  var span = document.createElement('span');
  span.innerHTML = s;
  return span.textContent || span.innerText;
};
var htmlDecode = function (input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}
var updateBG = function(){
  $('[data-background]').each(function(){
    var target =$(this).find('img').first();
    var bgURL= 'url('+target.attr('src')+')';
    $(this).css('background-image',bgURL);
    target.hide();
  });
}
window.done[0]=true;
window.done[1]=true;

window.done[4]=true;
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
$(function(){
  var balanceHeight = function(parent,container=null,offset=0){
  var maxHeight = 0;
  if(container==null)
  var item = parent.children();
  else
    var item = container.children();
  item.each(function (i, el) {
    if ($(this).height() > maxHeight)
      maxHeight = $(this).height();
  });
  var maxItemHeight = maxHeight + offset + 'px';
  item.css('height', maxItemHeight);
  if(container)
    container.css('height', maxItemHeight);
} 
  var expId = getUrlParameter('id');
  if(expId===undefined){
    window.location = "./index.html";
  }
  var settings = {
  "async": true,
  "crossDomain": true,
  "url": "/wordpress/index.php/wp-json/wp/v2/posts/"+expId+"?_embed",
  "method": "GET",
  "headers": {}
}
  var MONTH = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
  var projs = '';
$.ajax(settings).done(function (response) {
  let exp = response;
  var id='';
var s = exp.metadata.projects[0].split(';');
  let img = "/wordpress/wp-content/uploads/"+exp._embedded['wp:featuredmedia']['0'].media_details.file;
    let img_alt=exp._embedded['wp:featuredmedia']['0'].title;
s.forEach(function(e,i){
  if(i%2!=0){
    id += ','+e.split(':').slice(2,3);
    
  }
});
  projs = id.substring(1,id.length).replace('"','');
   var start_date_yr = exp.metadata.start_date.toString().substring(0,4);
    var end_date_yr = exp.metadata.end_date.toString().substring(0,4);
    var start_date_month = MONTH[parseInt(exp.metadata.start_date.toString().substring(4,6))-1];
    var end_date_month = exp.metadata.end_date.toString().substring(4,6);
    
    if(end_date_month=='')
      end_date_month='Present';
    else
      end_date_month=MONTH[parseInt(end_date_month)-1];
    var ofp= start_date_month+" "+start_date_yr+" - "+end_date_month+" "+end_date_yr;
  let cn = htmlDecode(exp.title.rendered).split(' | ')[0];
  let ds = htmlDecode(exp.title.rendered).split(' | ')[1];
  let disc= exp.metadata.short_descr.toString();
   
  let skil = exp._embedded['wp:term']['1'];
  let skilz='';
  skil.map((sk)=>{skilz+='<span class="skill">'+sk.name+'</span>'});
  $('[data-id="description"]').html(exp.content.rendered);
  $('[data-id="short-desc"]').html(disc);
  $('[data-id="designation"]').html(ds);
  $('[data-id="company-name"]').html(cn);
  $('[data-id="yr"]').html(ofp);
  $('[data-id="skills"]').html(skilz);
   $("[data-id='featured-img']").attr('src',img);
  $("[data-id='featured-img']").attr('alt',img_alt);
  window.done[2]=true;
  
});
  
  var settingsP = {
  "async": true,
  "crossDomain": true,
  "url": "/wordpress/index.php/wp-json/wp/v2/posts?_embed&categories=5&include="+projs,
  "method": "GET",
  "headers": {}
}

$.ajax(settingsP).done(function (response) {
  var proj = response;
  var projs = '';
  var tbnb = (proj.length > 0) ? '' : 'd-none';
  $('#project').addClass(tbnb);
  proj.forEach(function(it,ind){
    let img, img_alt;
  if (it._embedded['wp:featuredmedia']['0'] != null && it._embedded['wp:featuredmedia']['0'] != 'undefined') {
    img = it._embedded['wp:featuredmedia']['0'];
    img_alt = it._embedded['wp:featuredmedia']['0'].title;
  }
  else{
    img = 'img/wip.jpg';
    img_alt = 'No Image';
  }
  let txt = it.title.rendered;
  let skil = it._embedded['wp:term']['1'];
  let id = it.id;
  let skilz='';
  skil.map((sk)=>{skilz+='<span class="skill">'+sk.name+'</span>'});
  let template='<div class="project"> <div class="project-image" data-background> <img src="'+img+'" data-background alt="'+img_alt+'"> </div><div class="project-text"> <div><h4>'+txt+'</h4> <div class="skills">'+skilz+'</div></div><div><a href="project.html?id='+id+'" class="link">SEE DETAILS</a></div></div></div>';
  projs+=template;
  });
  $('[data-id="projects"]').html(projs);
  balanceHeight($('[data-id="projects"]'),null,60);
  updateBG();
  window.done[3]=true;
});

});