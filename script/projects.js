var extractContent = function (s) {
  var span = document.createElement('span');
  span.innerHTML = s;
  return span.textContent || span.innerText;
};
var updateBG = function(){
  $('[data-background]').each(function(){
    var target =$(this).find('img').first();
    var bgURL= 'url('+target.attr('src')+')';
    $(this).css('background-image',bgURL);
    target.hide();
  });
}
 
window.done[0]=true;
window.done[1]=true;
window.done[2]=true;
window.done[4]=true;
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
$(function(){
  var balanceHeight = function(parent,container=null,offset=0){
  var maxHeight = 0;
  if(container==null)
  var item = parent.children();
  else
    var item = container.children();
  item.each(function (i, el) {
    if ($(this).height() > maxHeight)
      maxHeight = $(this).height();
  });
  var maxItemHeight = maxHeight + offset + 'px';
  item.css('height', maxItemHeight);
  if(container)
    container.css('height', maxItemHeight);
} 
  var settingsP = {
  "async": true,
  "crossDomain": true,
  "url": "/wordpress/index.php/wp-json/wp/v2/posts?_embed&categories=5",
  "method": "GET",
  "headers": {}
}

$.ajax(settingsP).done(function (response) {
  var proj = response;
  var projs = '';
  var tbnb = (proj.length > 0) ? '' : 'd-none';
  $('#project').addClass(tbnb);
  proj.forEach(function(it,ind){
    let img, img_alt;
  if (it._embedded['wp:featuredmedia']['0'] != null && it._embedded['wp:featuredmedia']['0'] != 'undefined') {
    img = "/wordpress/wp-content/uploads/"+it._embedded['wp:featuredmedia']['0'].media_details.file;
    img_alt = it._embedded['wp:featuredmedia']['0'].title;
  }
  else{
    img = 'img/wip.jpg';
    img_alt = 'No Image';
  }
  let txt = it.title.rendered;
  let skil = it._embedded['wp:term']['1'];
  let id = it.id;
  let skilz='';
  skil.map((sk)=>{skilz+='<span class="skill">'+sk.name+'</span>'});
  let template='<div class="project"> <div class="project-image" data-background> <img src="'+img+'" data-background alt="'+img_alt+'"> </div><div class="project-text"> <div><h4>'+txt+'</h4> <div class="skills">'+skilz+'</div></div><div><a href="project.html?id='+id+'" class="link">SEE DETAILS</a></div></div></div>';
  projs+=template;
  });
  $('[data-id="projects"]').html(projs);

  window.done[3]=true;
  balanceHeight($('[data-id="projects"]'),null,60);
  updateBG();
});

});