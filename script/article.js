var extractContent = function (s) {
  var span = document.createElement('span');
  span.innerHTML = s;
  return span.textContent || span.innerText;
};
var updateBG = function(){
  $('[data-background]').each(function(){
    var target =$(this).find('img').first();
    var bgURL= 'url('+target.attr('src')+')';
    $(this).css('background-image',bgURL);
    target.hide();
  });
}
window.done[0]=true;
window.done[1]=true;
window.done[2]=true;
window.done[3]=true;

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
$(function(){
  var artId = getUrlParameter('id');
  if(artId===undefined){
    window.location = "./articles.html";
  }
  
  var settings = {
  "async": true,
  "crossDomain": true,
  "url":"/wordpress/index.php/wp-json/wp/v2/posts/"+artId+"?_embed",
  "method": "GET",
  "headers": {}
}

var MONTH = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
$.ajax(settings).done(function (response) {
  var art = response;
let img = "/wordpress/wp-content/uploads/"+art._embedded['wp:featuredmedia']['0'].media_details.file;
    let img_alt=art._embedded['wp:featuredmedia']['0'].title;
    let title= art.title.rendered;
    let id = art.id;
    let sd= art.excerpt.rendered;
    let dt = new Date(art.date).getDate()+" "+MONTH[new Date(art.date).getUTCMonth()-1]+' '+new Date(art.date).getFullYear();
  let skil = art._embedded['wp:term']['1'];
  let skilz = '';
  let body= art.content.rendered;
  skil.map((sk)=>{skilz+='<span class="keyword">'+sk.name+'</span>'});
  
  $('title').append(title);
  $("[data-id='title']").text(title);
  $("[data-id='keywords']").html(skilz);
  $("[data-id='date']").html(dt);
  $("[data-id='short-description']").html(sd);
  $("[data-id='body']").html(body);
  $("[data-id='featured-img']").attr('src',img);
  $("[data-id='featured-img']").attr('alt',img_alt);
  
//  updateBG();
  console.log(response);
  window.done[4]=true;
});

});